package junit;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * 
 * @author Aaron
 *Curso: 2018-2019 DAMIX-1B
 */
public class proves {
/**
 * Ejercicio JUnit Test.
 */
	@Test
    public void testJunit() {
		int[] entrada1 = { 4, 1, 3, 4, 10, 2, 4, 2, 9, 2, 8, 6, 12};
		int[] entrada2 = { 5, 6, 4, 4, 11, 2, 2, 8, 5, 2, 8, 4, 14};
		int[] entrada3 = { 12, 12, 12, 8, 12, 10, 2, 5, 10, 12, 8, 2, 6};
		String[] salida = { "SOBREN", "FALTEN", "EXACTE", "EXACTE", "SOBREN", "FALTEN", "EXACTE", "SOBREN", "SOBREN", "FALTEN", "EXACTE", "EXACTE", "EXACTE" };
		int i = 0;
		int casos = 20;
		while (casos != 0) {
			i = (int) (Math.random() * 12);
			String resultado = mandarines.Contar(entrada1[i],entrada2[i],entrada3[i]);
			String esperado = salida[i];
			assertEquals(esperado, resultado);
			casos--;
			}
		}

}