package junit;

import java.util.Scanner;
/**
 * 
 * @author Aaron
 *
 */
public class mandarinesSalida {
	static Scanner sc = new Scanner(System.in);
	static int veces = 25;
	static int num1 = 6;
	static int num2 = 0;
	static int num3 = 5;

	public static void main(String[] args) {
		int casos = veces;
		while (casos != 0) {
			System.out.println(Contar(num1, num2, num3));
			casos--;
		}
	}

	public static String Contar(int i, int j, int k) {

		int mandarines = i;
		int grills = j;
		int alumnes = k;
		int total = mandarines * grills;
		if (alumnes < total && multiple(total, alumnes) == false) {
			num1++;
			num2++;
			num3++;
			return "SOBREN";
		}
		if (alumnes > total) {
			num1++;
			num2++;
			num3++;
			return "FALTEN";
		}
		if (alumnes == total || multiple(total, alumnes) == true) {
			num1++;
			num2++;
			num3++;
			return "EXACTE";
		}
		
		return null;

	}

	public static boolean multiple(int total, int alumnes) {
		if (total % alumnes == 0) {
			return true;
		} else {
			return false;
		}
	}
}