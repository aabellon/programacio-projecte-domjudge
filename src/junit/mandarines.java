package junit;

import java.util.Scanner;
/**
 * 
 * @author Aaron
 *
 */
public class mandarines {
	static Scanner sc = new Scanner(System.in);
/**
 * Este metodo es nuestro main que hace las llamadas a metodo "Contar".
 * @param args Hace llamada a contar.
 */
	public static void main(String[] args) {
		int casos = sc.nextInt();
		while (casos != 0) {
			System.out.println(Contar(sc.nextInt(), sc.nextInt(), sc.nextInt()));
			casos--;
		}
	}
/**
 * Este metodo lo utilizamos para hacer los calculos de nuestras mandarinas y hace uso del metodo boolean llamado multiple.
 * @param i  Se refiere a nuestras mandarinas.
 * @param j  Se refiere a los gajos de nuestras mandarinas.
 * @param k  Se refiere al numero de alumnos que repartimos nuestras mandarinas.
 * @return Retornamos un string que informa si sobran, faltan o son exactos.
 */
	public static String Contar(int i, int j, int k) {

		int mandarines = i;
		int grills = j;
		int alumnes = k;
		int total = mandarines * grills;
		if (alumnes < total && multiple(total, alumnes) == false) {
			return "SOBREN";
		}
		if (alumnes > total) {
			return "FALTEN";
		}
		if (alumnes == total || multiple(total, alumnes) == true) {
			return "EXACTE";
		}
		return null;

	}
/**
 * Este metodo es un metodo boolean que utilizamos para saber si es multiple o no.
 * @param total El total es mandarinas por gajos.
 * @param alumnes El total de alumnos que aplicamos en nuestro ejercicio.
 * @return Retornamos un boolean que sera True si es multiplo o false si no lo es.
 */
	public static boolean multiple(int total, int alumnes) {
		if (total % alumnes == 0) {
			return true;
		} else {
			return false;
		}
	}
}